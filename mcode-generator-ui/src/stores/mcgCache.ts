import {defineStore} from 'pinia';
import {Session} from '/@/utils/storage';

/**
 * 缓存数据源ID
 */
export const useDataSourceId = defineStore('dataSourceId', {
    state: () => ({
        cacheDataSourceId: undefined,
    }),
    actions: {
        async getDataSourceId() {
            if (Session.get('dataSourceId')) {
                this.cacheDataSourceId = Session.get('dataSourceId');
            }
        },
        async setDataSourceId(data:number) {
            Session.set('dataSourceId',data);
        }
    },
});
