import request from '/@/utils/request';

export function codeTemplateApi() {
    return {
        // 分页查询
        queryTemplate: () => {
            return request({
                url: '/codeTemplate/list',
                method: 'get',
            });
        },
        // 保存 更新
        saveOrUpdate: (data:object) => {
            return request({
                url: '/codeTemplate/editOrAdd',
                method: 'post',
                data,
            })
        },
        // 删除
        deleteById: (id:any) => {
            return request({
                url: '/codeTemplate/deleteById?id=' + id,
                method: 'get',
            })
        }
    }
}

export function generatorApi() {
    return {
        // 分页查询
        queryDataSourcePage: (id: any) => {
            return request({
                url: '/generator/queryTables?id=' + id,
                method: 'get',
            });
        },
        // 生成代码
        code: (data: object) => {
            return request({
                responseType:'blob',
                url: '/generator/code',
                method: 'post',
                data,
            });
        },
        // 预览
        preview: (data:object) => {
            return request({
                url: '/generator/preview',
                method: 'post',
                data,
            });
        }
    }
}

export function dataSourceApi() {
    return {
        // 分页查询
        queryDataSourcePage: (data: object) => {
            return request({
                url: '/dataSource/queryPage',
                method: 'post',
                data,
            });
        },
        // 新增或修改
        addOrEdit: (data: Object) => {
            return request({
                url: '/dataSource/addOrEdit',
                method: 'post',
                data,
            });
        },
        // 测试连接
        testConnect: (data: Object) => {
            return request({
                url: '/dataSource/test',
                method: 'post',
                data,
            })
        },
        // 删除数据源
        delete: (id: number) => {
            return request({
                url: '/dataSource/deleteById?id=' + id,
                method: 'get',
            });
        },
        // list
        list: (data: object) => {
            return request({
                url: '/dataSource/list',
                method: 'post',
                data
            })
        },
    }
}

export function columnTypeMappingApi() {
    return {
        // 分页查询
        queryPage: (data: object) => {
            return request({
                url: '/columnTypeMapping/queryPage',
                method: 'post',
                data,
            });
        },
        // 新增或修改
        addOrEdit: (data: Object) => {
            return request({
                url: '/columnTypeMapping/addOrEdit',
                method: 'post',
                data,
            });
        },

        // 删除数据源
        batchDelete: (data: any) => {
            return request({
                url: '/columnTypeMapping/batchDeleteById',
                method: 'post',
                data
            });
        },
        // 恢复预设
        resetMapping: (data:string) => {
            return request({
                url: '/columnTypeMapping/resetMapping?databaseType=' + data,
                method: 'get',
            });
        }
    }
}

export function systemParameterApi() {
    return {
        // list
        list: (type:string,key:string) => {
            return request({
                url: `/systemParameter/list?type=${type}&key=${key}`,
                method: 'get',
            })
        },
        // 新增或修改
        edit: (data: Object) => {
            return request({
                url: '/systemParameter/edit',
                method: 'post',
                data,
            });
        },
        // 批量删除
        batchDelete:(data:object) => {
            return request({
                url: '/systemParameter/batchDelete',
                method: 'post',
                data,
            });
        }
    }
}