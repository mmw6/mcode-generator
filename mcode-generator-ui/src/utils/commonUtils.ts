export const DB = [
    {label:"SQLite",value:"SQLite"},
    {label:"MySQL",value:"MySQL"},
    {label:"Oracle",value:"Oracle"},
    {label:"SQLServer",value:"SQLServer"},
]

export const DB_CONSTANT = {
    MySQL: 'MySQL',
    SQLite: 'SQLite',
    Oracle: 'Oracle',
    SQLServer: 'SQLServer',
}

export const CONNECT_TYPE = {
    Default: 'Default',
    SID: 'SID',
    ServiceName: 'Service Name',
    URLOnly: 'URL Only',
    TNS: 'TNS',
}