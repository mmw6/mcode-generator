package com.mmw.mcodegenerator.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author: meimengwu
 * @time: 2024-04-19 14:48
 * @Description: com.mmw.mcodegenerator.entity 将模板内容存在数据库中，方便存取修改
 */

@Data
@TableName(value = "mcg_code_template")
public class CodeTemplate {
    @TableId
    private Long id;
    private String content;
    private String templateName;
    private String packageName;
    private String nameConverter;
    private String fileType;
}
