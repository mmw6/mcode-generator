package com.mmw.mcodegenerator.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author: meimengwu
 * @time: 2024/4/10 22:28
 * @Description: com.mmw.mcodegenerator.entity
 */
@Data
@TableName(value = "MCG_DATA_SOURCE")
public class DataSource {


    @TableId
    private Long id;
    private String databaseType;
    private String connectName;
    private String url;
    private String userName;
    private String userPassword;

    private String ip;

    private String port;

    // Oracle SID/ServiceName
    private String serviceName;

    private String databaseName;

    /**
     * DB路径 SQLite专用
     */
    private String dbPath;

    /**
     * 连接类型
     * Default URL only SID ServiceName TNS
     */
    private String connectType;

    @TableField(fill = FieldFill.INSERT)
    private String createTime;
}
