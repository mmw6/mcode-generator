package com.mmw.mcodegenerator.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author: meimengwu
 * @time: 2024-04-14 10:35
 * @Description: com.mmw.mcodegenerator.entity
 */
@Data
@TableName(value = "MCG_COLUMN_TYPE_MAPPING")
public class ColumnTypeMapping {

    @TableId
    private Long id;
    private String columnType;
    private String mappingType;
    private String packageName;
    private String databaseType;
    private String type;

    @TableField(fill = FieldFill.INSERT)
    private String createTime;
}
