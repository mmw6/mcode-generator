package com.mmw.mcodegenerator.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @author: meimengwu
 * @time: 2024-04-20 20:27
 * @Description: com.mmw.mcodegenerator.entity
 */

@TableName("MCG_SYSTEM_PARAMETER")
public class SystemParameter {

    @TableId
    private Long id;
    private String paramKey;
    private String paramValue;

    // 1 字符串类型 0 布尔类型
    private String paramType;
    private String enable;
    private String paramRemark;

    // SYS 系统参数 FIY 自定义参数
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getParamKey() {
        return paramKey;
    }

    public void setParamKey(String paramKey) {
        this.paramKey = paramKey;
    }

    public Object getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getEnable() {
        return this.enable;
    }

    public boolean paramEnable() {
        return "Y".equalsIgnoreCase(this.enable);
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    public String getParamRemark() {
        return paramRemark;
    }

    public void setParamRemark(String paramRemark) {
        this.paramRemark = paramRemark;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
