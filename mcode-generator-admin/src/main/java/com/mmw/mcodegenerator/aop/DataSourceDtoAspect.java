package com.mmw.mcodegenerator.aop;

import com.mmw.mcodegenerator.bean.dto.DataSourceDto;
import com.mmw.mcodegenerator.config.DatabaseType;
import com.mmw.mcodegenerator.generator.DatabaseFactoryProvider;
import com.mmw.mcodegenerator.generator.databasefactory.DatabaseFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

/**
 * @author: meimengwu
 * @time: 2024-04-17 17:08
 * @Description: com.mmw.mcodegenerator.aop
 */

@Component
@Aspect
public class DataSourceDtoAspect {
    @Autowired
    private DatabaseFactoryProvider databaseFactoryProvider;

    @Around("execution(* com.mmw.mcodegenerator.controller..*(..)) && args(dataSourceDto))")
    public Object interceptMethod(ProceedingJoinPoint joinPoint, DataSourceDto dataSourceDto) throws Throwable {
        if (ObjectUtils.isEmpty(dataSourceDto.getDb())) {
            dataSourceDto.setDb(DatabaseType.getByType(dataSourceDto.getDatabaseType()));
        }
        if (ObjectUtils.isEmpty(dataSourceDto.getFactory())) {
            DatabaseFactory factory = databaseFactoryProvider.build(dataSourceDto.getDatabaseType());
            dataSourceDto.setFactory(factory);
        }
        return joinPoint.proceed();
    }
}
