package com.mmw.mcodegenerator.exception;

import com.mmw.mcodegenerator.bean.dto.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: meimengwu
 * @time: 2024/4/12 21:45
 * @Description: com.mmw.mcodegenerator.exception
 */
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result<Object> exceptionHandler(Exception e) {
        log.error("全局异常捕获>>>", e);
        return Result.error(e.getMessage());
    }
}
