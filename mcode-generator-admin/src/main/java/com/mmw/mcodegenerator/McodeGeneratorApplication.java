package com.mmw.mcodegenerator;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.mmw.mcodegenerator.mapper")
public class McodeGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(McodeGeneratorApplication.class, args);
    }

}
