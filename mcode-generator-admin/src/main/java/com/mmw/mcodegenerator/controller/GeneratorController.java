package com.mmw.mcodegenerator.controller;

import com.mmw.mcodegenerator.bean.dto.DataSourceDto;
import com.mmw.mcodegenerator.bean.dto.Result;
import com.mmw.mcodegenerator.bean.qo.CodeQO;
import com.mmw.mcodegenerator.generator.dto.CodeDto;
import com.mmw.mcodegenerator.generator.dto.TableDefinitions;
import com.mmw.mcodegenerator.service.DataSourceService;
import com.mmw.mcodegenerator.utils.ZipUtil;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author: meimengwu
 * @time: 2024-04-14 17:07
 * @Description: com.mmw.mcodegenerator.controller
 */

@RestController
@RequestMapping("/generator")
public class GeneratorController {

    @Resource
    private DataSourceService dataSourceService;

    @GetMapping("/queryTables")
    public Result<List<TableDefinitions>> queryTables(Long id) throws Exception {
        DataSourceDto dataSource = dataSourceService.getDataSourceById(id);
        if (ObjectUtils.isEmpty(dataSource)) {
            return Result.error("未查询到数据源!");
        }
        return Result.success(dataSource.getFactory().getTableDefinition(dataSource));
    }

    @PostMapping("/code")
    public void code(@RequestBody CodeQO qo, HttpServletResponse response) throws Exception {
        List<CodeDto> code = dataSourceService.code(qo);
        // 设置响应的内容类型为 zip 文件
        try {
            response.setContentType("application/zip");
            response.setHeader("Content-Disposition", "attachment; filename=output.zip");

            // 获取 HttpServletResponse 的输出流
            try (OutputStream zipOut = response.getOutputStream()) {
                ZipUtil.addToZipStream(code, zipOut);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @PostMapping("/preview")
    public Result<Map<String, List<CodeDto>>> preview(@RequestBody CodeQO qo) throws Exception {
        List<CodeDto> code = dataSourceService.code(qo);
        Map<String, List<CodeDto>> result = code.stream().collect(Collectors.groupingBy(CodeDto::getFilePath));
        return Result.success(result);
    }

}
