package com.mmw.mcodegenerator.controller;

import com.mmw.mcodegenerator.bean.dto.Result;
import com.mmw.mcodegenerator.entity.CodeTemplate;
import com.mmw.mcodegenerator.service.CodeTemplateService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: meimengwu
 * @time: 2024-04-19 14:50
 * @Description: com.mmw.mcodegenerator.controller
 */
@RestController
@RequestMapping("/codeTemplate")
public class CodeTemplateController {

    @Resource
    private CodeTemplateService codeTemplateService;

    @GetMapping("/list")
    public Result<List<CodeTemplate>> listAll() {
        return Result.success(codeTemplateService.queryAll());
    }

    @PostMapping("/editOrAdd")
    @Transactional
    public Result<Object> editOrAdd(@RequestBody CodeTemplate codeTemplate) {
        codeTemplateService.saveOrUpdate(codeTemplate);
        return Result.success();
    }

    @GetMapping("/deleteById")
    @Transactional
    public Result<Object> deleteById(Long id) {
        codeTemplateService.removeById(id);
        return Result.success();
    }
}
