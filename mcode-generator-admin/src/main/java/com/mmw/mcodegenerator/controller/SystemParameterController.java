package com.mmw.mcodegenerator.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.mmw.mcodegenerator.bean.dto.Result;
import com.mmw.mcodegenerator.entity.SystemParameter;
import com.mmw.mcodegenerator.service.SystemParameterService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: meimengwu
 * @time: 2024-04-20 20:33
 * @Description: com.mmw.mcodegenerator.controller
 */


@RestController
@RequestMapping("/systemParameter")
public class SystemParameterController {

    @Resource
    private SystemParameterService systemParameterService;

    @GetMapping("/list")
    public Result<List<SystemParameter>> list(@RequestParam("type") String type, @RequestParam("key") String key) {
        return Result.success(systemParameterService.list(new LambdaQueryWrapper<SystemParameter>()
                .eq(SystemParameter::getType, type)
                .like(SystemParameter::getParamKey, key)));
    }

    @PostMapping("/edit")
    @Transactional
    public Result<Object> edit(@RequestBody SystemParameter entity) {
        long count = systemParameterService.count(new LambdaQueryWrapper<SystemParameter>()
                .eq(SystemParameter::getType, entity.getType())
                .eq(SystemParameter::getParamKey, entity.getParamKey())
                .ne(SystemParameter::getId, entity.getId()));
        if (count > 0) {
            return Result.error("已存在相同KEY参数");
        }
        systemParameterService.saveOrUpdate(entity);
        return Result.success();
    }

    @PostMapping("/batchDelete")
    @Transactional
    public Result<Object> batchDelete(@RequestBody List<Long> ids) {
        boolean result = systemParameterService.remove(new LambdaQueryWrapper<SystemParameter>().in(SystemParameter::getId, ids).eq(SystemParameter::getType, "DIY"));
        return result ? Result.success() : Result.error();
    }
}
