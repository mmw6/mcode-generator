package com.mmw.mcodegenerator.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mmw.mcodegenerator.bean.dto.DataSourceDto;
import com.mmw.mcodegenerator.bean.dto.Result;
import com.mmw.mcodegenerator.bean.qo.DataSourceQO;
import com.mmw.mcodegenerator.entity.DataSource;
import com.mmw.mcodegenerator.service.DataSourceService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: meimengwu
 * @time: 2024/4/11 0:23
 * @Description: com.mmw.mcodegenerator.controller
 */
@RestController
@RequestMapping("/dataSource")
public class DataSourceController {
    @Resource
    private DataSourceService dataSourceService;
    @Value("${ENCRYPT-KEY}")
    private String KEY;

    @PostMapping("/queryPage")
    public Result<IPage<DataSourceDto>> queryPage(@RequestBody DataSourceQO qo) throws Exception {
        return Result.success(dataSourceService.queryPage(qo));
    }

    @PostMapping("/addOrEdit")
    public Result<Object> add(@RequestBody DataSourceDto dto) {
        return dataSourceService.addOrEdit(dto);
    }

    @PostMapping("/list")
    public Result<List<DataSource>> list(@RequestBody DataSourceQO qo) throws Exception {
        List<DataSource> list = dataSourceService.list(qo);
        return Result.success(list);
    }

    @PostMapping("/test")
    public Result<Object> testConnect(@RequestBody DataSourceDto dto) throws Exception {
        return dataSourceService.testConnect(dto);
    }

    @GetMapping("/deleteById")
    @Transactional
    public Result<Object> deleteById(Long id) {
        boolean b = dataSourceService.removeById(id);
        return b ? Result.success() : Result.error();
    }
}
