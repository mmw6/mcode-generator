package com.mmw.mcodegenerator.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author: meimengwu
 * @time: 2024-04-22 4:05
 * @Description: com.mmw.mcodegenerator.controller
 */

@Controller
@RequestMapping
public class HomeController {

    @GetMapping("/")
    public String index() {
        return "index.html";
    }
}
