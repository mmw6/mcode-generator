package com.mmw.mcodegenerator.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mmw.mcodegenerator.bean.dto.Result;
import com.mmw.mcodegenerator.bean.qo.ColumnTypeMappingQO;
import com.mmw.mcodegenerator.entity.ColumnTypeMapping;
import com.mmw.mcodegenerator.service.ColumnTypeMappingService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: meimengwu
 * @time: 2024/4/14 10:23
 * @Description: com.mmw.mcodegenerator.controller
 */
@RestController
@RequestMapping("/columnTypeMapping")
public class ColumnTypeMappingController {
    @Resource
    private ColumnTypeMappingService columnTypeMappingService;

    @PostMapping("/queryPage")
    public Result<IPage<ColumnTypeMapping>> queryPage(@RequestBody ColumnTypeMappingQO qo) {
        return Result.success(columnTypeMappingService.queryPage(qo));
    }

    @PostMapping("/addOrEdit")
    public Result<Object> add(@RequestBody ColumnTypeMapping dto) {
        return columnTypeMappingService.addOrEdit(dto);
    }

    @PostMapping("/batchDeleteById")
    public Result<Object> batchDeleteById(@RequestBody List<Long> ids) {
        columnTypeMappingService.batchDeleteById(ids);
        return Result.success();
    }

    @GetMapping("/resetMapping")
    public Result<Object> resetMapping(String databaseType) {
        return columnTypeMappingService.resetMapping(databaseType);
    }

}
