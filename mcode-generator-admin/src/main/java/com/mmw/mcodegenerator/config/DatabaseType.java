package com.mmw.mcodegenerator.config;

import org.springframework.util.ObjectUtils;

/**
 * 目前支持的数据库
 *
 * @author: meimengwu
 * @time: 2024/4/12 21:17
 * @Description: com.mmw.mcodegenerator.config
 */
public enum DatabaseType {
    SQLite("SQLite", "org.sqlite.JDBC", "jdbc:sqlite:%s", "db/sqlite_mapping.sql"),
    MySQL("MySQL", "com.mysql.cj.jdbc.Driver", "jdbc:mysql://%s:%s/%s?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai", "db/mysql_mapping.sql"),
    SQLServer("SQLServer", "com.microsoft.sqlserver.jdbc.SQLServerDriver", "jdbc:sqlserver://%s:%s;DatabaseName=%s", ""),
    Oracle("Oracle", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@%s:%s:%s", "db/oracle_mapping.sql"),
    EMPTY("EMPTY", "null", "null", ""),
    ;

    private final String databaseType;
    private final String driverClassName;
    private final String urlTemplate;
    private final String defaultMappingFile;

    DatabaseType(String databaseType, String driverClassName, String urlTemplate, String defaultMappingFile) {
        this.databaseType = databaseType;
        this.driverClassName = driverClassName;
        this.urlTemplate = urlTemplate;
        this.defaultMappingFile = defaultMappingFile;
    }

    public String getDatabaseType() {
        return this.databaseType;
    }

    public String getDriverClassName() {
        return this.driverClassName;
    }

    public String getUrlTemplate() {
        return urlTemplate;
    }

    public String getDefaultMappingFile() {
        return defaultMappingFile;
    }


    public static DatabaseType getByType(String databaseType) {
        DatabaseType type = DatabaseType.valueOf(databaseType);
        return ObjectUtils.isEmpty(type) ? DatabaseType.EMPTY : type;
    }
}
