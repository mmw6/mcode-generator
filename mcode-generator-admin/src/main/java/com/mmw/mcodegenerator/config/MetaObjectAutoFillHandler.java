package com.mmw.mcodegenerator.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.mmw.mcodegenerator.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author: meimengwu
 * @time: 2024/4/11 9:20
 * @Description: com.mmw.mcodegenerator.config
 */
@Slf4j
@Component
public class MetaObjectAutoFillHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("start insert fill...");
        this.setFieldValByName("createTime", DateUtils.formatDateTime(new Date()), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {

    }
}
