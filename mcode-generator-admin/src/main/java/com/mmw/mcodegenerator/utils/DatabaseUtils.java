package com.mmw.mcodegenerator.utils;

import com.mmw.mcodegenerator.bean.dto.DataSourceDto;
import com.mmw.mcodegenerator.bean.dto.Result;
import com.mmw.mcodegenerator.config.DatabaseType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbcp2.BasicDataSourceFactory;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * 数据库工具类
 *
 * @author: meimengwu
 * @time: 2024/4/12 21:34
 * @Description: com.mmw.mcodegenerator.utils
 */
@Slf4j
public class DatabaseUtils {

    private static final Map<String, DataSource> DATA_SOURCE_MAP = new ConcurrentHashMap<>(16);
    private static final ThreadLocal<Map<String, Connection>> CONNECTION_LOCAL = new ThreadLocal<>();

    public static List<String> getSupports() {
        return Arrays.stream(DatabaseType.values())
                .map(DatabaseType::getDatabaseType)
                .collect(Collectors.toList());
    }

    public static Result<Object> testGetConnection(DataSourceDto dataSourceDto) {
        Connection connection = null;
        try {
            Class.forName(dataSourceDto.getDb().getDriverClassName());
            connection = DriverManager.getConnection(dataSourceDto.getUrl(), dataSourceDto.getUserName(), dataSourceDto.getUserPassword());
            if (DatabaseType.SQLite.getDatabaseType().equalsIgnoreCase(dataSourceDto.getDatabaseType())) {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM sqlite_master");
                if (resultSet.isClosed()) {
                    return Result.error("数据库不存在");
                } else {
                    return Result.success();
                }
            }
            return connection != null ? Result.success() : Result.error("获取连接为空!");
        } catch (Exception ex) {
            log.error("获取连接失败", ex);
            return Result.error(String.format("获取连接失败:%s", ex.getMessage()));
        } finally {
            closeConnection(connection);
        }
    }

    public static Connection getConnection(DataSourceDto dataSource) throws Exception {
        String key = dataSource.getKey();
        Map<String, Connection> map = CONNECTION_LOCAL.get();
        Connection connection;
        if (map == null) {
            map = new HashMap<>();
        }

        if (map.containsKey(key)) {
            connection = map.get(key);
        } else {
            try {
                connection = getDataSource(dataSource).getConnection();
                map.put(key, connection);
                CONNECTION_LOCAL.set(map);
            } catch (Exception e) {
                log.error("获取连接失败,", e);
                throw e;
            }
        }
        return connection;
    }

    public static boolean closeConnection(String key) {
        Connection connection = CONNECTION_LOCAL.get().get(key);
        if (connection != null) {
            return closeConnection(connection);
        }
        CONNECTION_LOCAL.remove();
        return true;
    }

    public static boolean closeConnection(Connection connection) {
        if (connection == null) {
            return true;
        }
        try {
            connection.close();
            return true;
        } catch (SQLException e) {
            log.error("连接关闭异常", e);
            return false;
        }
    }

    public static DataSource getDataSource(DataSourceDto dataSourceConfig) throws Exception {
        String jdbcUrl = dataSourceConfig.getDatabaseType() + ":" + dataSourceConfig.getUrl();
        DataSource dataSource = DATA_SOURCE_MAP.computeIfAbsent(jdbcUrl, key -> {
            Properties properties = new Properties();
            properties.put("driverClassName", dataSourceConfig.getDb().getDriverClassName());
            properties.put("url", dataSourceConfig.getUrl());
            properties.put("username", dataSourceConfig.getUserName());
            properties.put("password", dataSourceConfig.getUserPassword());
            // 初始连接数
            properties.put("initialSize", 1);
            // 最大活跃数
            properties.put("maxTotal", 30);
            properties.put("minIdle", 5);
            properties.put("maxIdle", 10);
            // 最长等待时间(毫秒)
            properties.put("maxWaitMillis", 1000);
            // 程序中的连接不使用后是否被连接池回收
            properties.put("removeAbandonedOnMaintenance", true);
            properties.put("removeAbandonedOnBorrow", true);
            // 连接在所指定的秒数内未使用才会被删除(秒)
            properties.put("removeAbandonedTimeout", 5);
            try {
                return BasicDataSourceFactory.createDataSource(properties);
            } catch (Exception e) {
                log.error("获取数据源异常:", e);
                return null;
            }
        });
        if (dataSource == null) {
            throw new Exception("连接数据库失败");
        }
        return dataSource;
    }
}
