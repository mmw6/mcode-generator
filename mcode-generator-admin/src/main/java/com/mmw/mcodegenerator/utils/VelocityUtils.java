package com.mmw.mcodegenerator.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.stereotype.Component;

import java.io.StringWriter;
import java.util.Map;

/**
 * @author: meimengwu
 * @time: 2024-04-19 14:35
 * @Description: com.mmw.mcodegenerator.utils Velocity工具类
 */
@Slf4j
@Component
public class VelocityUtils {

    private final VelocityEngine velocityEngine;

    public VelocityUtils() {
        this.velocityEngine = new VelocityEngine();
        // 使用 StringResourceLoader 加载模板字符串
        velocityEngine.setProperty("resource.loader", "string");
        velocityEngine.setProperty("string.resource.loader.class", "org.apache.velocity.runtime.resource.loader.StringResourceLoader");
        this.velocityEngine.init();
    }

    public String generatorCode(String templateString, Map<String, Object> param) {
        try {
            // 使用 VelocityContext 将数据传递给模板
            VelocityContext context = new VelocityContext();
            for (Map.Entry<String, Object> entry : param.entrySet()) {
                context.put(entry.getKey(), entry.getValue());
            }

            // 将模板和数据结合生成文件内容
            StringWriter writer = new StringWriter();
            velocityEngine.evaluate(context, writer, "velocity_log", templateString);
            return writer.toString();
        } catch (Exception e) {
            log.error("生成代码异常", e);
            throw e;
        }
    }
}
