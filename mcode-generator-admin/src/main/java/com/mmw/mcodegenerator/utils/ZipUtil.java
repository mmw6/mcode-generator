package com.mmw.mcodegenerator.utils;

import com.mmw.mcodegenerator.generator.dto.CodeDto;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipUtil {

    public static void addToZipStream(List<CodeDto> codeList, OutputStream outputStream) throws IOException {
        try (ZipOutputStream zipOut = new ZipOutputStream(outputStream)) {
            for (CodeDto codeDto : codeList) {
                addToZip(codeDto, zipOut);
            }
        }
    }

    private static void addToZip(CodeDto codeDto, ZipOutputStream zipOut) throws IOException {
        String filePath = codeDto.getFilePath().replace('.', '/');
        filePath = filePath.replaceAll("//", "/");
        if (!filePath.isEmpty() && !filePath.endsWith("/")) {
            filePath += "/";
        }

        ZipEntry zipEntry = new ZipEntry(filePath + codeDto.getFileName());
        zipOut.putNextEntry(zipEntry);
        byte[] bytes = codeDto.getContent().getBytes();
        zipOut.write(bytes, 0, bytes.length);
        zipOut.closeEntry();
    }
}
