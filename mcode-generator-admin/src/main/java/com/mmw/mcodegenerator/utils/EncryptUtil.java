package com.mmw.mcodegenerator.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * @author: meimengwu
 * @time: 2024-04-14 0:07
 * @Description: com.mmw.mcodegenerator.utils
 */
@Component
public class EncryptUtil {

    @Value("${ENCRYPT-KEY}")
    private String KEY;

    private static final String ALGORITHM = "AES";

    private static final String AES_ALGORITHM = "AES/ECB/PKCS5Padding";

    public String encrypt(String data) throws Exception {
        if (StringUtils.isEmpty(data)) {
            return "";
        }
        Cipher cipher = getCipher();
        return Base64.getEncoder().encodeToString(cipher.doFinal(data.getBytes(StandardCharsets.UTF_8)));
    }

    public String decrypt(String data) throws Exception {
        if (StringUtils.isEmpty(data)) {
            return "";
        }
        Cipher cipher = getCipher();
        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(KEY.getBytes(), ALGORITHM));
        byte[] decode = Base64.getDecoder().decode(data.getBytes(StandardCharsets.UTF_8));
        byte[] bytes = cipher.doFinal(decode);
        return new String(bytes, StandardCharsets.UTF_8);
    }

    private Cipher getCipher() throws Exception {
        KeyGenerator instance = KeyGenerator.getInstance(ALGORITHM);
        instance.init(128);
        SecretKeySpec secretKeySpec = new SecretKeySpec(KEY.getBytes(), ALGORITHM);
        Cipher cipher = Cipher.getInstance(AES_ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
        return cipher;
    }
}
