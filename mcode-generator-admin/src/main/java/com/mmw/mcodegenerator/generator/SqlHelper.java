package com.mmw.mcodegenerator.generator;

import com.mmw.mcodegenerator.bean.dto.DataSourceDto;
import com.mmw.mcodegenerator.utils.DatabaseUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: meimengwu
 * @time: 2024-04-17 10:38
 * @Description: com.mmw.mcodegenerator.generator
 */
@Component
@Slf4j
public class SqlHelper {

    /**
     * 执行SQL语句并获取结果集List<Map<String,Object> Map中的COLUMN KEY为全大写
     *
     * @param sql           查询SQL
     * @param dataSourceDto 数据源信息
     * @return 结果集封装List
     * @throws SQLException SQL异常
     */
    public List<Map<String, Object>> queryRowList(String sql, DataSourceDto dataSourceDto) throws SQLException {
        Connection connection;
        try {
            connection = DatabaseUtils.getConnection(dataSourceDto);
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();
            List<Map<String, Object>> resultList = new ArrayList<>();
            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();
            while (rs.next()) {
                Map<String, Object> rowMap = new HashMap<>();
                for (int i = 1; i <= columnCount; i++) {
                    String columnName = metaData.getColumnName(i);
                    Object columnValue = rs.getObject(i);
                    rowMap.put(columnName.toUpperCase(), columnValue);
                }
                resultList.add(rowMap);
            }
            return resultList;
        } catch (Exception ex) {
            log.error("SQL执行异常", ex);
            DatabaseUtils.closeConnection(dataSourceDto.getKey());
            throw new SQLException("SQL执行异常", ex);
        }
    }
}
