package com.mmw.mcodegenerator.generator.dto;

import lombok.Data;

/**
 * @author: meimengwu
 * @time: 2024-04-19 18:34
 * @Description: com.mmw.mcodegenerator.bean.dto
 */
@Data
public class CodeDto {
    String content;

    String filePath;

    String fileName;
}
