package com.mmw.mcodegenerator.generator.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.List;
import java.util.Set;

/**
 * @author: meimengwu
 * @time: 2024-04-17 10:28
 * @Description: com.mmw.mcodegenerator.generator.dto 表信息定义字段统一
 */
@Data
public class TableDefinitions {

    /**
     * 原始表名
     */
    private String tableName;

    private String tableNameCamel;

    /**
     * 驼峰首字母大写 表名
     * capitalizeFirstLetterTableName
     */
    private String cflTableName;

    /**
     * 表备注
     */
    private String tableComment;

    /**
     * 表创建时间
     */
    private String createTime;

    /**
     * 表中字段类型全限定名
     */
    @TableField(exist = false)
    private Set<String> columnDataTypePackage;

    @TableField(exist = false)
    private List<ColumnDefinitions> columns;

}
