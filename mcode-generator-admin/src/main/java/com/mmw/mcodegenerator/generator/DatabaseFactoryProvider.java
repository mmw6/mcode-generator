package com.mmw.mcodegenerator.generator;

import com.mmw.mcodegenerator.generator.databasefactory.DatabaseFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author: meimengwu
 * @time: 2024-04-17 11:10
 * @Description: com.mmw.mcodegenerator.generator
 */
@Component
@Slf4j
public class DatabaseFactoryProvider {
    @Resource
    private BeanFactory beanFactory;

    public DatabaseFactory build(String name) throws Exception {
        DatabaseFactory instance;
        try {
            instance = beanFactory.getBean(name, DatabaseFactory.class);
            if (instance == null) {
                throw new Exception("找不到数据库服务实例,请确认是否支持" + name + "数据库！");
            }
        } catch (Exception ex) {
            log.error("DatabaseFactoryProvider：" + ex);
            throw new Exception("获取" + name + "数据库服务实例异常,请确认接口编码是否正确！");
        }
        return instance;
    }
}
