package com.mmw.mcodegenerator.generator.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * @author: meimengwu
 * @time: 2024-04-19 18:35
 * @Description: com.mmw.mcodegenerator.generator.dto
 */
@Data
public class ColumnDefinitions {

    /**
     * 是否主键
     */
    private boolean primaryKey;

    /**
     * 原始字段名
     */
    private String originalColumnName;

    /**
     * 驼峰风格字段名
     */
    private String camelTypeColumnName;

    @TableField(exist = false)
    private String cflColumnName;

    /**
     * 字段类型
     */
    private String columnType;


    /**
     * 字段类型类
     */
    private String columnTypeClass;

    /**
     * 是否可为null
     */
    private boolean nullable;

    /**
     * 字段顺序
     */
    private long order;

    /**
     * 字段最大长度
     */
    private long maxLength;

    /**
     * 字段备注
     */
    private String columnComment;

}
