package com.mmw.mcodegenerator.generator.databasefactory;

import com.mmw.mcodegenerator.bean.dto.DataSourceDto;
import com.mmw.mcodegenerator.generator.dto.CodeDto;
import com.mmw.mcodegenerator.generator.dto.ColumnDefinitions;
import com.mmw.mcodegenerator.generator.dto.TableDefinitions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author: meimengwu
 * @time: 2024-05-01 下午5:34
 * @Description: com.mmw.mcodegenerator.generator.databasefactory
 */
@Component("Oracle")
@Slf4j
public class OracleFactory extends BasicFactory implements DatabaseFactory{
    @Override
    public String buildSqlForQueryTables(DataSourceDto dataSource) {
        return "SELECT * FROM USER_TAB_COMMENTS";
    }

    @Override
    public String buildSqlForQueryColumns(DataSourceDto dataSource) {
        return "";
    }

    @Override
    public List<TableDefinitions> getTableDefinition(DataSourceDto dataSource) throws SQLException {
        List<Map<String, Object>> resultList = sqlHelper.queryRowList(buildSqlForQueryTables(dataSource), dataSource);
        List<TableDefinitions> tableDefinitionsList = new ArrayList<>();
        if (!ObjectUtils.isEmpty(resultList)) {
            for (Map<String, Object> map : resultList) {
                TableDefinitions tableDefinitions = new TableDefinitions();
                tableDefinitions.setTableName(convertString(map.get(getTableNameKey())));
                tableDefinitions.setTableComment(convertString(map.get(getTableCommentKey())));
                tableDefinitions.setCreateTime(convertString(map.get(getTableCreateTimeKey())));
                tableDefinitionsList.add(tableDefinitions);
            }
        }
        return tableDefinitionsList;
    }

    @Override
    public Map<String, List<ColumnDefinitions>> getColumnDefinitionMap(DataSourceDto dataSourceDto) throws Exception {
        return Collections.emptyMap();
    }

    @Override
    public String getTableNameKey() {
        return "TABLE_NAME";
    }

    @Override
    public String getTableCommentKey() {
        return "COMMENTS";
    }

    @Override
    public String getTableCreateTimeKey() {
        return "";
    }

    @Override
    public List<CodeDto> code(DataSourceDto dataSourceDto) throws Exception {
        return Collections.emptyList();
    }
}
