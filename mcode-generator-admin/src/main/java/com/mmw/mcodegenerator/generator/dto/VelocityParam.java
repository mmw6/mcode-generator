package com.mmw.mcodegenerator.generator.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author: meimengwu
 * @time: 2024-04-21 0:29
 * @Description: com.mmw.mcodegenerator.generator.dto
 */

@Data
public class VelocityParam {
    /**
     * 项目包名
     */
    private String basePackagePath;

    /**
     * 作者名
     */
    private String author;

    /**
     * 作者邮箱
     */
    private String email;

    /**
     * 表定义
     */
    private TableDefinitions table;

    /**
     * 生成时间
     */
    private String dateTime;

    /**
     * 是否启用Lombok
     */
    private boolean enableLombok;

    /**
     * 文件名/类名
     */
    private String className;

    /**
     * 列定义信息
     */
    private List<ColumnDefinitions> columns;

    private Map<String,Object> param;
}
