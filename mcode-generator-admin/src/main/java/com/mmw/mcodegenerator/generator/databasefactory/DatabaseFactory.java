package com.mmw.mcodegenerator.generator.databasefactory;

import com.mmw.mcodegenerator.bean.dto.DataSourceDto;
import com.mmw.mcodegenerator.generator.dto.CodeDto;
import com.mmw.mcodegenerator.generator.dto.ColumnDefinitions;
import com.mmw.mcodegenerator.generator.dto.TableDefinitions;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @author: meimengwu
 * @time: 2024-04-15 8:19
 * @Description: com.mmw.mcodegenerator.generator
 */
public interface DatabaseFactory {

    /**
     * 构建查询数据库表信息SQL
     *
     * @return 查询SQL
     */
    String buildSqlForQueryTables(DataSourceDto dataSource);

    String buildSqlForQueryColumns(DataSourceDto dataSource);

    /**
     * 查询数据库中的表信息
     *
     * @param dataSource 数据源信息
     * @return 表信息List
     * @throws SQLException SQL异常
     */
    List<TableDefinitions> getTableDefinition(DataSourceDto dataSource) throws Exception;

    Map<String, List<ColumnDefinitions>> getColumnDefinitionMap(DataSourceDto dataSourceDto) throws Exception;

    String getTableNameKey();

    String getTableCommentKey();

    String getTableCreateTimeKey();

    List<CodeDto> code(DataSourceDto dataSourceDto) throws Exception;
}
