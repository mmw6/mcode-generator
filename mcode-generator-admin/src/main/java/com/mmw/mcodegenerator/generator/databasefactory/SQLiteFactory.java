package com.mmw.mcodegenerator.generator.databasefactory;

import com.mmw.mcodegenerator.bean.dto.DataSourceDto;
import com.mmw.mcodegenerator.entity.ColumnTypeMapping;
import com.mmw.mcodegenerator.generator.SqlHelper;
import com.mmw.mcodegenerator.generator.dto.CodeDto;
import com.mmw.mcodegenerator.generator.dto.ColumnDefinitions;
import com.mmw.mcodegenerator.generator.dto.TableDefinitions;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author: meimengwu
 * @time: 2024-04-17 18:08
 * @Description: com.mmw.mcodegenerator.generator.databasefactory
 */
@Component("SQLite")
public class SQLiteFactory extends BasicFactory implements DatabaseFactory {

    @Resource
    private SqlHelper sqlHelper;

    @Override
    public String buildSqlForQueryTables(DataSourceDto dataSource) {
        return "SELECT * FROM sqlite_master WHERE TYPE = 'table'";
    }

    @Override
    public String buildSqlForQueryColumns(DataSourceDto dataSource) {
        String sql = "SELECT tbl_name,sql FROM sqlite_master";
        if (!CollectionUtils.isEmpty(dataSource.getTables())) {
            String collect = dataSource.getTables().stream().map(table -> String.format("'%s'", table.getTableName())).collect(Collectors.joining(","));
            sql += String.format(" WHERE tbl_name IN (%s)", collect);
        }
        return sql;
    }

    @Override
    public List<TableDefinitions> getTableDefinition(DataSourceDto dataSourceDto) throws Exception {
        List<Map<String, Object>> resultList = sqlHelper.queryRowList(buildSqlForQueryTables(dataSourceDto), dataSourceDto);
        List<TableDefinitions> tableDefinitionsList = new ArrayList<>();
        if (!ObjectUtils.isEmpty(resultList)) {
            for (Map<String, Object> map : resultList) {
                TableDefinitions tableDefinitions = new TableDefinitions();
                tableDefinitions.setTableName(convertString(map.get(getTableNameKey())));
                tableDefinitions.setTableComment("");
                tableDefinitions.setCreateTime("");
                tableDefinitionsList.add(tableDefinitions);
            }
        }
        return tableDefinitionsList;
    }

    @Override
    public Map<String, List<ColumnDefinitions>> getColumnDefinitionMap(DataSourceDto dataSourceDto) throws Exception {
        // 查询映射关系
        Map<String, ColumnTypeMapping> mappingMap = columnTypeMappingService.queryMappingMap(dataSourceDto.getDatabaseType());
        String sql = buildSqlForQueryColumns(dataSourceDto);
        List<Map<String, Object>> maps = sqlHelper.queryRowList(sql, dataSourceDto);
        Map<String, List<ColumnDefinitions>> resultMap = new HashMap<>();
        for (Map<String, Object> map : maps) {
            String tableName = convertString(map.get("TBL_NAME"));
            String ddlSql = convertString(map.get("SQL"));
            // 解析SQL
            Map<String, ColumnInfo> columnMap = parseColumnNamesAndTypes(ddlSql);
            List<ColumnDefinitions> list = new ArrayList<>();
            for (Map.Entry<String, ColumnInfo> entry : columnMap.entrySet()) {
                String columnName = entry.getKey();
                ColumnInfo columnInfo = entry.getValue();
                ColumnDefinitions columnDefinitions = new ColumnDefinitions();
                columnDefinitions.setPrimaryKey(columnInfo.primaryKey);
                columnDefinitions.setOriginalColumnName(columnName);
                columnDefinitions.setCamelTypeColumnName(toCamelCase(columnName, false));
                // 驼峰首字母大写风格
                columnDefinitions.setCflColumnName(toCamelCase(columnName, true));
                String dataType = columnInfo.getType();
                // 约定字段类型统一为大写
                if (mappingMap.containsKey(dataType.toUpperCase())) {
                    ColumnTypeMapping type = mappingMap.get(dataType.toUpperCase());
                    columnDefinitions.setColumnType(type.getMappingType());
                    // java.lang 不需要import
                    if (!ObjectUtils.isEmpty(type.getPackageName()) && !type.getPackageName().startsWith("java.lang")) {
                        columnDefinitions.setColumnTypeClass(type.getPackageName());
                    }
                } else {
                    columnDefinitions.setColumnType("unKnow");
                    columnDefinitions.setColumnTypeClass(null);
                }
                columnDefinitions.setNullable(false);
                columnDefinitions.setOrder(columnInfo.getOrder());
                columnDefinitions.setColumnComment("");
                list.add(columnDefinitions);
            }
            resultMap.put(tableName, list);
        }
        return resultMap;
    }

    // 列信息类
    static class ColumnInfo {
        private String type;
        private boolean primaryKey;

        private int order;

        public ColumnInfo(String type, boolean primaryKey, int order) {
            this.type = type;
            this.primaryKey = primaryKey;
            this.order = order;
        }

        public int getOrder() {
            return order;
        }

        public void setOrder(int order) {
            this.order = order;
        }

        public String getType() {
            return type;
        }

        public boolean isPrimaryKey() {
            return primaryKey;
        }
    }

    // 解析字段名和字段类型
    public static Map<String, ColumnInfo> parseColumnNamesAndTypes(String createTableStatement) {
        Map<String, ColumnInfo> columnMap = new HashMap<>();

        // 去除换行符和多余空格
        createTableStatement = createTableStatement.replaceAll("\\s+", " ").trim();

        // 查找字段定义部分
        int startIndex = createTableStatement.indexOf("(");
        int endIndex = createTableStatement.lastIndexOf(")");
        if (startIndex >= 0 && endIndex >= 0) {
            String columnsBlock = createTableStatement.substring(startIndex + 1, endIndex);

            // 按逗号分割字段定义
            String[] columns = columnsBlock.split(",");
            int order = 0;
            for (String column : columns) {
                column = column.trim();
                if (!column.isEmpty()) {
                    // 按空格分割字段名和字段类型
                    String[] parts = column.split("\\s+");
                    if (parts.length >= 2) {
                        String columnName = parts[0];
                        String columnType = parts[1];
                        boolean isPrimaryKey = column.toUpperCase().contains("PRIMARY KEY");
                        columnMap.put(columnName, new ColumnInfo(columnType, isPrimaryKey, order));
                        order++;
                    }
                }
            }
        }

        return columnMap;
    }

    @Override
    public String getTableNameKey() {
        return "TBL_NAME";
    }

    @Override
    public String getTableCommentKey() {
        return null;
    }

    @Override
    public String getTableCreateTimeKey() {
        return null;
    }

    @Override
    public List<CodeDto> code(DataSourceDto dataSourceDto) throws Exception {
        Map<String, List<ColumnDefinitions>> columnDefinitionMap = getColumnDefinitionMap(dataSourceDto);
        return generatorCodeList(dataSourceDto, columnDefinitionMap);
    }
}
