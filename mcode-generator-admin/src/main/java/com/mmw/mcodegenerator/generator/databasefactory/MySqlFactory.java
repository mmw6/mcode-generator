package com.mmw.mcodegenerator.generator.databasefactory;

import com.mmw.mcodegenerator.bean.dto.DataSourceDto;
import com.mmw.mcodegenerator.entity.ColumnTypeMapping;
import com.mmw.mcodegenerator.generator.dto.CodeDto;
import com.mmw.mcodegenerator.generator.dto.ColumnDefinitions;
import com.mmw.mcodegenerator.generator.dto.TableDefinitions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author: meimengwu
 * @time: 2024-04-15 9:03
 * @Description: com.mmw.mcodegenerator.generator.databasefactory
 */
@Component("MySQL")
@Slf4j
public class MySqlFactory extends BasicFactory implements DatabaseFactory {

    @Override
    public String buildSqlForQueryTables(DataSourceDto dataSource) {
        String databaseName = dataSource.getDatabaseName();
        return String.format("SELECT * FROM information_schema.tables WHERE TABLE_SCHEMA = '%s'", databaseName);
    }

    @Override
    public String buildSqlForQueryColumns(DataSourceDto dataSource) {
        String databaseName = dataSource.getDatabaseName();
        String sql = String.format("SELECT * from information_schema.COLUMNS WHERE TABLE_SCHEMA = '%s'", databaseName);
        if (!CollectionUtils.isEmpty(dataSource.getTables())) {
            String collect = dataSource.getTables().stream().map(table -> String.format("'%s'", table.getTableName())).collect(Collectors.joining(","));
            sql += String.format(" AND TABLE_NAME IN (%s) ORDER BY ORDINAL_POSITION", collect);
        }
        return sql;
    }

    @Override
    public List<TableDefinitions> getTableDefinition(DataSourceDto dataSourceDto) throws Exception {
        List<Map<String, Object>> resultList = sqlHelper.queryRowList(buildSqlForQueryTables(dataSourceDto), dataSourceDto);
        List<TableDefinitions> tableDefinitionsList = new ArrayList<>();
        if (!ObjectUtils.isEmpty(resultList)) {
            for (Map<String, Object> map : resultList) {
                TableDefinitions tableDefinitions = new TableDefinitions();
                tableDefinitions.setTableName(convertString(map.get(getTableNameKey())));
                tableDefinitions.setTableComment(convertString(map.get(getTableCommentKey())));
                tableDefinitions.setCreateTime(convertString(map.get(getTableCreateTimeKey())));
                tableDefinitionsList.add(tableDefinitions);
            }
        }
        return tableDefinitionsList;
    }

    @Override
    public Map<String, List<ColumnDefinitions>> getColumnDefinitionMap(DataSourceDto dataSourceDto) throws Exception {
        // 查询映射关系
        Map<String, ColumnTypeMapping> mappingMap = columnTypeMappingService.queryMappingMap(dataSourceDto.getDatabaseType());
        List<Map<String, Object>> maps = sqlHelper.queryRowList(buildSqlForQueryColumns(dataSourceDto), dataSourceDto);
        Map<String, List<ColumnDefinitions>> resultMap = new HashMap<>();
        Map<String, List<Map<String, Object>>> collect = maps.stream().collect(Collectors.groupingBy(o -> convertString(o.get(getTableNameKey()))));
        for (Map.Entry<String, List<Map<String, Object>>> entry : collect.entrySet()) {
            List<ColumnDefinitions> list = new ArrayList<>();
            String tableName = entry.getKey();
            List<Map<String, Object>> columns = entry.getValue();
            for (Map<String, Object> column : columns) {
                ColumnDefinitions columnDefinitions = new ColumnDefinitions();
                columnDefinitions.setPrimaryKey("PRI".equals(convertString(column.get("COLUMN_KEY"))));
                columnDefinitions.setOriginalColumnName(convertString(column.get("COLUMN_NAME")));
                columnDefinitions.setCamelTypeColumnName(toCamelCase(convertString(column.get("COLUMN_NAME")), false));
                // 驼峰首字母大写风格
                columnDefinitions.setCflColumnName(toCamelCase(convertString(column.get("COLUMN_NAME")),true));
                String dataType = convertString(column.get("DATA_TYPE"));
                // 约定字段类型统一为大写
                if (mappingMap.containsKey(dataType.toUpperCase())) {
                    ColumnTypeMapping type = mappingMap.get(dataType.toUpperCase());
                    columnDefinitions.setColumnType(type.getMappingType());
                    // java.lang 不需要import
                    if (!ObjectUtils.isEmpty(type.getPackageName()) && !type.getPackageName().startsWith("java.lang")) {
                        columnDefinitions.setColumnTypeClass(type.getPackageName());
                    }
                } else {
                    columnDefinitions.setColumnType("unKnow");
                    columnDefinitions.setColumnTypeClass(null);
                }
                columnDefinitions.setNullable("YES".equalsIgnoreCase(convertString(column.get("IS_NULLABLE"))));
                columnDefinitions.setOrder((long) column.get("ORDINAL_POSITION"));
                Object obj = column.get("CHARACTER_MAXIMUM_LENGTH");
                if (!ObjectUtils.isEmpty(obj)) {
                    columnDefinitions.setMaxLength((long) obj);
                }
                columnDefinitions.setColumnComment(convertString(column.get("COLUMN_COMMENT")));
                list.add(columnDefinitions);
            }
            resultMap.put(tableName, list);
        }
        return resultMap;
    }


    @Override
    public String getTableNameKey() {
        return "TABLE_NAME";
    }

    @Override
    public String getTableCommentKey() {
        return "TABLE_COMMENT";
    }

    @Override
    public String getTableCreateTimeKey() {
        return "CREATE_TIME";
    }

    @Override
    public List<CodeDto> code(DataSourceDto dataSourceDto) throws Exception {
        Map<String, List<ColumnDefinitions>> columnDefinitionMap = getColumnDefinitionMap(dataSourceDto);
        return generatorCodeList(dataSourceDto,columnDefinitionMap);
    }
}
