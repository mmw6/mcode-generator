package com.mmw.mcodegenerator.generator.databasefactory;

import com.mmw.mcodegenerator.bean.dto.DataSourceDto;
import com.mmw.mcodegenerator.entity.CodeTemplate;
import com.mmw.mcodegenerator.entity.SystemParameter;
import com.mmw.mcodegenerator.generator.SqlHelper;
import com.mmw.mcodegenerator.generator.dto.CodeDto;
import com.mmw.mcodegenerator.generator.dto.ColumnDefinitions;
import com.mmw.mcodegenerator.generator.dto.TableDefinitions;
import com.mmw.mcodegenerator.generator.dto.VelocityParam;
import com.mmw.mcodegenerator.service.ColumnTypeMappingService;
import com.mmw.mcodegenerator.service.SystemParameterService;
import com.mmw.mcodegenerator.utils.DateUtils;
import com.mmw.mcodegenerator.utils.VelocityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author: meimengwu
 * @time: 2024-04-17 11:08
 * @Description: com.mmw.mcodegenerator.generator.databasefactory
 */
@Slf4j
public class BasicFactory {

    @Resource
    protected SystemParameterService systemParameterService;
    @Resource
    protected ColumnTypeMappingService columnTypeMappingService;
    @Autowired
    protected VelocityUtils velocityUtils;
    @Resource
    protected SqlHelper sqlHelper;


    public static String convertString(Object object) {
        if (object == null) {
            return "";
        }
        return String.valueOf(object);
    }

    /**
     * SnakeCase to CamelCase
     *
     * @param fieldName             字段名
     * @param capitalizeFirstLetter 首字母是否大写
     * @return 转换后字段名
     */
    public static String toCamelCase(String fieldName, boolean capitalizeFirstLetter) {
        StringBuilder result = new StringBuilder();
        boolean capitalizeNext = capitalizeFirstLetter;
        for (int i = 0; i < fieldName.length(); i++) {
            char c = fieldName.charAt(i);
            if (c == '_') {
                capitalizeNext = true;
            } else {
                if (capitalizeNext) {
                    result.append(Character.toUpperCase(c));
                    capitalizeNext = false;
                } else {
                    result.append(Character.toLowerCase(c));
                }
            }
        }
        return result.toString();
    }

    public static String removePrefix(String text, String prefix) {
        if (text.startsWith(prefix)) {
            return text.substring(prefix.length());
        }
        return text;
    }

    public String generatorCode(String templateString, VelocityParam param) {
        Map<String, Object> map = new HashMap<>();
        map.put("Context",param);
        String code = velocityUtils.generatorCode(templateString, map);
        log.error("生成代码:{}",code);
        return code;
    }


    /**
     * 将字符串中 // 替换为 /
     *
     * @param path 字符串
     * @return 字符串
     */
    public static String removeDoubleSlashes(String path) {
        return removeDouble(path,"/");
    }

    public static String removeDouble(String path,String replacement) {
        return path.replaceAll("/{2,}", replacement);
    }

    public SystemParameter getBasePackageConfig(Map<Long, SystemParameter> map) {
        return map.get(1L);
    }

    public SystemParameter getRemovePrefixConfig(Map<Long, SystemParameter> map) {
        return map.get(2L);
    }

    public SystemParameter getAuthorConfig(Map<Long, SystemParameter> map) {
        return map.get(3L);
    }

    public SystemParameter getEmailConfig(Map<Long, SystemParameter> map) {
        return map.get(4L);
    }

    public boolean getEnableLombok(Map<Long, SystemParameter> map) {
        return "Y".equals(convertString(map.get(5L).getParamValue()));
    }


    public List<CodeDto> generatorCodeList(DataSourceDto dataSourceDto,Map<String, List<ColumnDefinitions>> columnDefinitionMap) {
        List<TableDefinitions> tables = dataSourceDto.getTables();
        List<CodeTemplate> templates = dataSourceDto.getTemplates();
        List<CodeDto> result = new ArrayList<>();

        // 获取系统参数
        Map<Long, SystemParameter> systemConfig = systemParameterService.param();
        // 获取DIY参数 集成到VelocityContext中
        Map<String, Object> diyParamMap = systemParameterService.diyParam();

        // 获取是否去除前缀
        SystemParameter removePrefixConfig = getRemovePrefixConfig(systemConfig);
        // 作者
        SystemParameter authorConfig = getAuthorConfig(systemConfig);
        String author = authorConfig.paramEnable() ? convertString(authorConfig.getParamValue()) : "";
        // 邮箱
        SystemParameter emailConfig = getEmailConfig(systemConfig);
        String email = emailConfig.paramEnable() ? convertString(emailConfig.getParamValue()) : "";
        // 项目包名
        SystemParameter basePackageConfig = getBasePackageConfig(systemConfig);
        String basePackage = basePackageConfig.paramEnable() ? convertString(basePackageConfig.getParamValue()) : "";

        for (TableDefinitions table : tables) {
            String tableName = table.getTableName();
            if (removePrefixConfig.paramEnable() && !ObjectUtils.isEmpty(convertString(removePrefixConfig.getParamValue()))) {
                tableName = removePrefix(tableName, convertString(removePrefixConfig.getParamValue()));
            }
            table.setTableNameCamel(toCamelCase(tableName, false));
            table.setCflTableName(toCamelCase(table.getTableName(), true));
            // 字段按order升序排序
            List<ColumnDefinitions> list = columnDefinitionMap.get(table.getTableName());
            Set<String> columnDataTypePackage = new HashSet<>();
            for (ColumnDefinitions columnDefinitions : list) {
                if (!ObjectUtils.isEmpty(columnDefinitions.getColumnTypeClass())) {
                    columnDataTypePackage.add(columnDefinitions.getColumnTypeClass());
                }
            }
            table.setColumnDataTypePackage(columnDataTypePackage);
            list = list.stream().sorted(Comparator.comparing(ColumnDefinitions::getOrder)).collect(Collectors.toList());
            table.setColumns(list);
            for (CodeTemplate template : templates) {
                CodeDto codeDto = new CodeDto();

                VelocityParam velocityParam = new VelocityParam();
                velocityParam.setAuthor(author);
                velocityParam.setEmail(email);
                velocityParam.setTable(table);
                velocityParam.setBasePackagePath(basePackage);
                velocityParam.setDateTime(DateUtils.formatDateTime(new Date()));
                velocityParam.setEnableLombok(getEnableLombok(systemConfig));
                velocityParam.setColumns(table.getColumns());
                velocityParam.setParam(diyParamMap);


                String className = generatorCode(template.getNameConverter(), velocityParam);
                String fileName = removeDouble(String.format("%s.%s",className,template.getFileType()),".");
                velocityParam.setClassName(className);
                // 文件路径 . 替换为 /  // 替换为 /
                codeDto.setFilePath(removeDoubleSlashes(basePackage + "." + template.getPackageName()));
                codeDto.setFileName(fileName);
                codeDto.setContent(generatorCode(template.getContent(), velocityParam));
                result.add(codeDto);
            }
        }
        return result;
    }
}
