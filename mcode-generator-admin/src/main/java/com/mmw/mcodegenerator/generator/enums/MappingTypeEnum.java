package com.mmw.mcodegenerator.generator.enums;

/**
 * java.lang不需要显示引入
 *
 * @author: meimengwu
 * @time: 2024-04-14 11:06
 * @Description: com.mmw.mcodegenerator.config
 */
public enum MappingTypeEnum {

    // 基本数据类型
    BYTE("byte", "java.lang"),
    SHORT("short", "java.lang"),
    INT("int", "java.lang"),
    LONG("long", "java.lang"),
    FLOAT("float", "java.lang"),
    DOUBLE("double", "java.lang"),
    BOOLEAN("boolean", "java.lang"),
    CHAR("char", "java.lang"),

    // 包装类
    BYTE_WRAPPER("Byte", "java.lang"),
    SHORT_WRAPPER("Short", "java.lang"),
    INT_WRAPPER("Integer", "java.lang"),
    LONG_WRAPPER("Long", "java.lang"),
    FLOAT_WRAPPER("Float", "java.lang"),
    DOUBLE_WRAPPER("Double", "java.lang"),
    BOOLEAN_WRAPPER("Boolean", "java.lang"),
    CHAR_WRAPPER("Character", "java.lang"),

    DATE("Date", "java.util.Date"),

    // 引用数据类型
    STRING("String", "java.lang"),
    BIG_DECIMAL("BigDecimal", "java.math"),
    ;


    private final String typeName;
    private final String packageName;


    MappingTypeEnum(String typeName, String packageName) {
        this.typeName = typeName;
        this.packageName = packageName;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getPackageName() {
        return packageName;
    }
}
