package com.mmw.mcodegenerator.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mmw.mcodegenerator.bean.dto.Result;
import com.mmw.mcodegenerator.bean.qo.ColumnTypeMappingQO;
import com.mmw.mcodegenerator.entity.ColumnTypeMapping;

import java.util.List;
import java.util.Map;

/**
 * @author: meimengwu
 * @time: 2024-04-14 10:38
 * @Description: com.mmw.mcodegenerator.service
 */
public interface ColumnTypeMappingService {


    /**
     * 分页查询
     *
     * @param qo 请求参数
     * @return 分页数据
     */
    IPage<ColumnTypeMapping> queryPage(ColumnTypeMappingQO qo);

    /**
     * 根据ID存在判断新增或者修改
     *
     * @param dto 新增/修改数据
     * @return Result
     */
    Result<Object> addOrEdit(ColumnTypeMapping dto);

    /**
     * 根据ID批量删除
     *
     * @param ids 删除数据ID集合
     */
    void batchDeleteById(List<Long> ids);

    List<ColumnTypeMapping> list(ColumnTypeMappingQO qo);

    /**
     * 重置数据库映射关系
     *
     * @param databaseType 数据库类型
     */
    Result<Object> resetMapping(String databaseType);

    Map<String, ColumnTypeMapping> queryMappingMap(String databaseType);
}
