package com.mmw.mcodegenerator.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mmw.mcodegenerator.bean.dto.DataSourceDto;
import com.mmw.mcodegenerator.bean.dto.Result;
import com.mmw.mcodegenerator.bean.qo.CodeQO;
import com.mmw.mcodegenerator.bean.qo.DataSourceQO;
import com.mmw.mcodegenerator.config.DatabaseType;
import com.mmw.mcodegenerator.entity.DataSource;
import com.mmw.mcodegenerator.generator.DatabaseFactoryProvider;
import com.mmw.mcodegenerator.generator.databasefactory.DatabaseFactory;
import com.mmw.mcodegenerator.generator.dto.CodeDto;
import com.mmw.mcodegenerator.mapper.DataSourceMapper;
import com.mmw.mcodegenerator.service.DataSourceService;
import com.mmw.mcodegenerator.utils.DatabaseUtils;
import com.mmw.mcodegenerator.utils.EncryptUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: meimengwu
 * @time: 2024/4/11 0:08
 * @Description: com.mmw.mcodegenerator.service.impl
 */
@Service
@Slf4j
public class DataSourceServiceImpl extends ServiceImpl<DataSourceMapper, DataSource> implements DataSourceService {
    @Resource
    private DataSourceMapper dataSourceMapper;
    @Resource
    private EncryptUtil encryptUtil;
    @Resource
    private DatabaseFactoryProvider databaseFactoryProvider;


    @Override
    public IPage<DataSourceDto> queryPage(DataSourceQO qo) throws Exception {
        Page<DataSource> page = new Page<>(qo.getPageNum(), qo.getPageSize());
        LambdaQueryWrapper<DataSource> wrapper = buildQueryWrapper(qo);
        page = dataSourceMapper.selectPage(page, wrapper);
        Page<DataSourceDto> result = new Page<>(qo.getPageNum(), qo.getPageSize());
        if (!CollectionUtils.isEmpty(page.getRecords())) {
            List<DataSourceDto> records = new ArrayList<>();
            for (DataSource o : page.getRecords()) {
                DataSourceDto dataSourceDto = new DataSourceDto();
                BeanUtils.copyProperties(o, dataSourceDto);
                dataSourceDto.setUserPassword(encryptUtil.decrypt(o.getUserPassword()));
                dataSourceDto.setLength(dataSourceDto.getUserPassword().length());
                records.add(dataSourceDto);
            }
            return result.setRecords(records);
        }
        return result;
    }

    @Override
    public List<DataSource> list(DataSourceQO qo) throws Exception {
        List<DataSource> list = this.list(buildQueryWrapper(qo));
        if (!CollectionUtils.isEmpty(list)) {
            for (DataSource dataSource : list) {
                dataSource.setUserPassword(encryptUtil.decrypt(dataSource.getUserPassword()));
            }
        }
        return list;
    }

    @SneakyThrows
    @Override
    @Transactional
    public Result<Object> addOrEdit(DataSourceDto dto) {
        List<String> support = DatabaseUtils.getSupports();
        if (!support.contains(dto.getDatabaseType())) {
            return Result.error(String.format("不支持的数据库类型:%s", dto.getDatabaseType()));
        }
        dto.setUserPassword(encryptUtil.encrypt(dto.getUserPassword()));
        if (ObjectUtils.isEmpty(dto.getId())) {
            super.save(dto);
        } else {
            super.updateById(dto);
        }
        return Result.success();
    }

    @Override
    public Result<Object> testConnect(DataSourceDto dto) {
        return DatabaseUtils.testGetConnection(dto);
    }

    private LambdaQueryWrapper<DataSource> buildQueryWrapper(DataSourceQO qo) {
        LambdaQueryWrapper<DataSource> wrapper = new LambdaQueryWrapper<>();
        if (!StringUtils.isEmpty(qo.getConnectName())) {
            wrapper.eq(DataSource::getConnectName, qo.getConnectName());
        }
        if (!StringUtils.isEmpty(qo.getDatabaseType())) {
            wrapper.eq(DataSource::getDatabaseType, qo.getDatabaseType());
        }
        return wrapper;
    }

    @Override
    public DataSourceDto getDataSourceById(Long id) throws Exception {
        DataSource source = super.getById(id);
        DataSourceDto target = new DataSourceDto();
        if (!ObjectUtils.isEmpty(source)) {
            BeanUtils.copyProperties(source, target);
            DatabaseFactory factory = databaseFactoryProvider.build(source.getDatabaseType());
            target.setFactory(factory);
            target.setDb(DatabaseType.getByType(target.getDatabaseType()));
            target.setUserPassword(encryptUtil.decrypt(target.getUserPassword()));
        }
        return target;
    }


    @Override
    public List<CodeDto> code(CodeQO dto) throws Exception {
        DataSourceDto dataSource = getDataSourceById(dto.getId());
        DatabaseFactory factory = dataSource.getFactory();
        dataSource.setTables(dto.getTables());
        dataSource.setTemplates(dto.getTemplates());
        return factory.code(dataSource);
    }
}
