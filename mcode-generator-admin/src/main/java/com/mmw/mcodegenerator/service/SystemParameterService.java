package com.mmw.mcodegenerator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mmw.mcodegenerator.entity.SystemParameter;

import java.util.Map;

/**
 * @author: meimengwu
 * @time: 2024-04-20 20:30
 * @Description: com.mmw.mcodegenerator.service
 */
public interface SystemParameterService extends IService<SystemParameter> {
    Map<Long, SystemParameter> param();

    Map<String,Object> diyParam();
}
