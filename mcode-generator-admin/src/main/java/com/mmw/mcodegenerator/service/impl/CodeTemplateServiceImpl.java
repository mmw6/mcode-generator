package com.mmw.mcodegenerator.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mmw.mcodegenerator.entity.CodeTemplate;
import com.mmw.mcodegenerator.mapper.CodeTemplateMapper;
import com.mmw.mcodegenerator.service.CodeTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: meimengwu
 * @time: 2024-04-19 14:52
 * @Description: com.mmw.mcodegenerator.service.impl
 */
@Service
@Slf4j
public class CodeTemplateServiceImpl extends ServiceImpl<CodeTemplateMapper, CodeTemplate> implements CodeTemplateService {
    @Override
    public List<CodeTemplate> queryAll() {
        return this.list();
    }
}
