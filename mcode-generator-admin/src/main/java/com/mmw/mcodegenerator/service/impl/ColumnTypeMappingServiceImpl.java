package com.mmw.mcodegenerator.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mmw.mcodegenerator.bean.dto.Result;
import com.mmw.mcodegenerator.bean.qo.ColumnTypeMappingQO;
import com.mmw.mcodegenerator.config.DatabaseType;
import com.mmw.mcodegenerator.entity.ColumnTypeMapping;
import com.mmw.mcodegenerator.mapper.ColumnTypeMappingMapper;
import com.mmw.mcodegenerator.service.ColumnTypeMappingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author: meimengwu
 * @time: 2024-04-14 10:39
 * @Description: com.mmw.mcodegenerator.service.impl
 */

@Service
@Slf4j
public class ColumnTypeMappingServiceImpl extends ServiceImpl<ColumnTypeMappingMapper, ColumnTypeMapping> implements ColumnTypeMappingService {

    @Resource
    private ColumnTypeMappingMapper columnTypeMappingMapper;
    @Resource
    private DataSource dataSource;

    @Override
    public IPage<ColumnTypeMapping> queryPage(ColumnTypeMappingQO qo) {
        Page<ColumnTypeMapping> page = new Page<>(qo.getPageNum(), qo.getPageSize());
        LambdaQueryWrapper<ColumnTypeMapping> wrapper = buildQueryWrapper(qo);
        return columnTypeMappingMapper.selectPage(page, wrapper);
    }

    @Override
    @Transactional
    public Result<Object> addOrEdit(ColumnTypeMapping dto) {
        // 数据库类型 + 字段类型 只能配置一种映射类型
        LambdaQueryWrapper<ColumnTypeMapping> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ColumnTypeMapping::getMappingType,dto.getMappingType());
        wrapper.eq(ColumnTypeMapping::getColumnType,dto.getColumnType());
        if (!ObjectUtils.isEmpty(dto.getId())) {
            wrapper.ne(ColumnTypeMapping::getId,dto.getId());
        }
        long count = this.count(wrapper);
        if (count > 0) {
            return Result.error("数据库类型 + 字段类型 只能配置一种映射类型");
        }
        // 字段类型统一全大写
        dto.setColumnType(dto.getColumnType().toUpperCase());
        if (ObjectUtils.isEmpty(dto.getId())) {
            super.save(dto);
        } else {
            super.updateById(dto);
        }
        return Result.success();
    }

    @Override
    @Transactional
    public void batchDeleteById(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return;
        }
        this.removeBatchByIds(ids);
    }

    @Override
    public List<ColumnTypeMapping> list(ColumnTypeMappingQO qo) {
        LambdaQueryWrapper<ColumnTypeMapping> wrapper = buildQueryWrapper(qo);
        return columnTypeMappingMapper.selectList(wrapper);
    }

    @Override
    public Result<Object> resetMapping(String databaseType) {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            // 加载执行SQL
            String file = DatabaseType.getByType(databaseType).getDefaultMappingFile();
            ClassPathResource resource = new ClassPathResource(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                if (!line.startsWith("-")) {
                    statement.addBatch(line);
                }
            }
            statement.executeBatch();
            return Result.success();
        } catch (Exception e) {
            log.error("执行出错", e);
            return Result.error("重置异常：" + e.getMessage());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                log.error("资源关闭异常", ex);
            }
        }
    }

    @Override
    public Map<String, ColumnTypeMapping> queryMappingMap(String databaseType) {
        ColumnTypeMappingQO qo = new ColumnTypeMappingQO();
        qo.setDatabaseType(databaseType);
        List<ColumnTypeMapping> mapping = this.list(qo);
        Map<String, ColumnTypeMapping> result = new HashMap<>();
        if (!CollectionUtils.isEmpty(mapping)) {
            result = mapping.stream().collect(Collectors.toMap(ColumnTypeMapping::getColumnType, Function.identity(), (v1, v2) -> v1));
        }
        return result;
    }

    private LambdaQueryWrapper<ColumnTypeMapping> buildQueryWrapper(ColumnTypeMappingQO qo) {
        LambdaQueryWrapper<ColumnTypeMapping> wrapper = new LambdaQueryWrapper<>();
        if (!ObjectUtils.isEmpty(qo.getDatabaseType())) {
            wrapper.eq(ColumnTypeMapping::getDatabaseType, qo.getDatabaseType());
        }
        if (!ObjectUtils.isEmpty(qo.getColumnType())) {
            wrapper.eq(ColumnTypeMapping::getColumnType, qo.getColumnType());
        }
        if (!ObjectUtils.isEmpty(qo.getMappingType())) {
            wrapper.like(ColumnTypeMapping::getMappingType, qo.getMappingType());
        }
        return wrapper;
    }
}
