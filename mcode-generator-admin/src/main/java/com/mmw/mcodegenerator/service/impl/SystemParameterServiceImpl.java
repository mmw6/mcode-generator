package com.mmw.mcodegenerator.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mmw.mcodegenerator.entity.SystemParameter;
import com.mmw.mcodegenerator.mapper.SystemParameterMapper;
import com.mmw.mcodegenerator.service.SystemParameterService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: meimengwu
 * @time: 2024-04-20 20:31
 * @Description: com.mmw.mcodegenerator.service.impl
 */
@Service
public class SystemParameterServiceImpl extends ServiceImpl<SystemParameterMapper, SystemParameter> implements SystemParameterService {


    @Override
    public Map<Long, SystemParameter> param() {
        new LambdaQueryWrapper<SystemParameter>().eq(SystemParameter::getType,"SYS");
        List<SystemParameter> list = this.list();
        Map<Long, SystemParameter> result = new HashMap<>();
        for (SystemParameter systemParameter : list) {
            result.put(systemParameter.getId(), systemParameter);
        }
        return result;
    }

    @Override
    public Map<String, Object> diyParam() {
        LambdaQueryWrapper<SystemParameter> diy = new LambdaQueryWrapper<SystemParameter>().eq(SystemParameter::getType, "DIY");
        List<SystemParameter> list = this.list(diy);
        Map<String, Object> result = new HashMap<>();
        for (SystemParameter v : list) {
            if ("1".equals(v.getParamType())) {
                result.put(v.getParamKey(),v.getParamValue());
            } else {
                boolean val = "Y".equals(v.getParamValue());
                result.put(v.getParamKey(),val);
            }
        }
        return result;
    }


}
