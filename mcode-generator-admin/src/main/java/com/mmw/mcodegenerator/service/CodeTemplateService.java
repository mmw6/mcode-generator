package com.mmw.mcodegenerator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mmw.mcodegenerator.entity.CodeTemplate;

import java.util.List;

/**
 * @author: meimengwu
 * @time: 2024-04-19 14:52
 * @Description: com.mmw.mcodegenerator.service
 */
public interface CodeTemplateService extends IService<CodeTemplate> {

    List<CodeTemplate> queryAll();
}
