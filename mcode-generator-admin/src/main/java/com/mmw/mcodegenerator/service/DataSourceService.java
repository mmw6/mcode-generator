package com.mmw.mcodegenerator.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mmw.mcodegenerator.bean.dto.DataSourceDto;
import com.mmw.mcodegenerator.bean.dto.Result;
import com.mmw.mcodegenerator.bean.qo.CodeQO;
import com.mmw.mcodegenerator.bean.qo.DataSourceQO;
import com.mmw.mcodegenerator.entity.DataSource;
import com.mmw.mcodegenerator.generator.dto.CodeDto;

import java.util.List;

/**
 * @author: meimengwu
 * @time: 2024/4/11 0:08
 * @Description: com.mmw.mcodegenerator.service
 */
public interface DataSourceService extends IService<DataSource> {

    /**
     * 分页查询
     *
     * @param qo 请求参数
     * @return 分页数据
     */
    IPage<DataSourceDto> queryPage(DataSourceQO qo) throws Exception;

    /**
     * 列表查询
     *
     * @param qo 请求参数
     * @return list
     */
    List<DataSource> list(DataSourceQO qo) throws Exception;

    /**
     * 新增或修改数据源
     *
     * @param dto 提交数据
     * @return Result
     */
    Result<Object> addOrEdit(DataSourceDto dto);

    /**
     * 测试连接
     *
     * @param dto 提交数据
     * @return Result
     */
    Result<Object> testConnect(DataSourceDto dto) throws Exception;

    /**
     * 获取数据源
     *
     * @param id
     * @return
     */
    DataSourceDto getDataSourceById(Long id) throws Exception;

    /**
     * 生成代码
     *
     * @param dto
     * @return
     */
    List<CodeDto> code(CodeQO dto) throws Exception;
}
