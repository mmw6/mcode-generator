package com.mmw.mcodegenerator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mmw.mcodegenerator.entity.CodeTemplate;

/**
 * @author: meimengwu
 * @time: 2024-04-19 14:51
 * @Description: com.mmw.mcodegenerator.mapper
 */
public interface CodeTemplateMapper extends BaseMapper<CodeTemplate> {
}
