package com.mmw.mcodegenerator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mmw.mcodegenerator.entity.DataSource;

/**
 * @author: meimengwu
 * @time: 2024/4/10 23:56
 * @Description: com.mmw.mcodegenerator.mapper
 */
public interface DataSourceMapper extends BaseMapper<DataSource> {
}
