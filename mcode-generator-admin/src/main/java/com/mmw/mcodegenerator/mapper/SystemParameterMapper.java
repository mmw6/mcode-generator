package com.mmw.mcodegenerator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mmw.mcodegenerator.entity.SystemParameter;

/**
 * @author: meimengwu
 * @time: 2024-04-20 20:32
 * @Description: com.mmw.mcodegenerator.mapper
 */
public interface SystemParameterMapper extends BaseMapper<SystemParameter> {
}
