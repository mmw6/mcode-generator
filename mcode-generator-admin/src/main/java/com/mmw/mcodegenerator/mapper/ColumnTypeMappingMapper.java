package com.mmw.mcodegenerator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mmw.mcodegenerator.entity.ColumnTypeMapping;

/**
 * @author: meimengwu
 * @time: 2024-04-14 10:39
 * @Description: com.mmw.mcodegenerator.mapper
 */
public interface ColumnTypeMappingMapper extends BaseMapper<ColumnTypeMapping> {
}
