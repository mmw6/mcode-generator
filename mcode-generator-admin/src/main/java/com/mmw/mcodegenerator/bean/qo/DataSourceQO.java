package com.mmw.mcodegenerator.bean.qo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author: meimengwu
 * @time: 2024/4/11 0:11
 * @Description: com.mmw.mcodegenerator.bean.qo
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DataSourceQO extends PageQO {

    /**
     * 数据库类型
     */
    private String databaseType;

    /**
     * 连接名称
     */
    private String connectName;

}
