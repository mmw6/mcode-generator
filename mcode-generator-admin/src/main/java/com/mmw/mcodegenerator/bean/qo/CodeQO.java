package com.mmw.mcodegenerator.bean.qo;

import com.mmw.mcodegenerator.entity.CodeTemplate;
import com.mmw.mcodegenerator.generator.dto.TableDefinitions;
import lombok.Data;

import java.util.List;

/**
 * @author: meimengwu
 * @time: 2024-04-20 18:32
 * @Description: com.mmw.mcodegenerator.bean.qo
 */
@Data
public class CodeQO {

    private Long id;
    /**
     * 表
     */
    private List<TableDefinitions> tables;

    /**
     * 模板
     */
    private List<CodeTemplate> templates;
}
