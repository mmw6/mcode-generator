package com.mmw.mcodegenerator.bean.qo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author: meimengwu
 * @time: 2024-04-14 10:54
 * @Description: com.mmw.mcodegenerator.bean.qo
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ColumnTypeMappingQO extends PageQO {

    private String databaseType;
    private String mappingType;
    private String columnType;
}
