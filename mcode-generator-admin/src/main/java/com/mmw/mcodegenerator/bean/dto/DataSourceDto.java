package com.mmw.mcodegenerator.bean.dto;

import com.mmw.mcodegenerator.config.DatabaseType;
import com.mmw.mcodegenerator.entity.CodeTemplate;
import com.mmw.mcodegenerator.entity.ColumnTypeMapping;
import com.mmw.mcodegenerator.entity.DataSource;
import com.mmw.mcodegenerator.generator.databasefactory.DatabaseFactory;
import com.mmw.mcodegenerator.generator.dto.TableDefinitions;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author: meimengwu
 * @time: 2024/4/11 0:10
 * @Description: com.mmw.mcodegenerator.bean
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class DataSourceDto extends DataSource {

    /**
     * 密码长度
     */
    private int length;

    /**
     * 数据库类型
     */
    private DatabaseType db;

    /**
     * 是否加密
     */
    private Boolean isEncrypt;

    /**
     * 抽象数据库工厂
     */
    private DatabaseFactory factory;

    /**
     * 表
     */
    private List<TableDefinitions> tables;

    /**
     * 模板
     */
    private List<CodeTemplate> templates;

    /**
     * 映射关系
     */
    private List<ColumnTypeMapping> columnTypeMappings;

    private String key;

    public String getKey() {
        return String.format("%s:%s", getUrl(), getUserName());
    }
}
