package com.mmw.mcodegenerator.bean.qo;

import lombok.Data;

/**
 * @author: meimengwu
 * @time: 2024/4/11 9:42
 * @Description: com.mmw.mcodegenerator.bean.qo
 */
@Data
public class PageQO {

    private int pageNum = 1;

    private int pageSize = 10;

    private int total;
}
