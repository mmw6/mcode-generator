package com.mmw.mcodegenerator.bean.dto;

import lombok.Data;

/**
 * @author: meimengwu
 * @time: 2024/4/11 0:25
 * @Description: com.mmw.mcodegenerator.bean.dto
 */
@Data
public class Result<T> {

    private int code;

    private String message;

    private T data;

    public static <T> Result<T> success() {
        return build(0, null, "处理成功");
    }

    public static <T> Result<T> success(T data) {
        return success(data, "处理成功");
    }

    public static <T> Result<T> success(T data, String message) {
        return build(0, data, message);
    }

    public static <T> Result<T> error() {
        return build(-1, null, "处理失败");
    }

    public static <T> Result<T> error(String message) {
        return build(-1, null, message);
    }

    public static <T> Result<T> build(int code, T data, String message) {
        Result<T> result = new Result<>();
        result.setData(data);
        result.setCode(code);
        result.setMessage(message);
        return result;
    }
}

