PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE MCG_DATA_SOURCE
(
    ID INTEGER PRIMARY KEY,
    DATABASE_TYPE TEXT NOT NULL,
    CONNECT_NAME TEXT NOT NULL,
    URL TEXT NOT NULL ,
    USER_NAME TEXT NOT NULL,
    USER_PASSWORD TEXT NOT NULL,
    CREATE_TIME TEXT NOT NULL
, IP TEXT, PORT TEXT, DATABASE_NAME TEXT, DB_PATH TEXT);
INSERT INTO MCG_DATA_SOURCE VALUES(1,'SQLite','SQLite测试','jdbc:sqlite:file:admin.db','','','2024-04-17 09:41:03','','','','file:admin.db');
CREATE TABLE MCG_COLUMN_TYPE_MAPPING
(
    ID            INTEGER primary key,
    COLUMN_TYPE   TEXT not null,
    MAPPING_TYPE  TEXT not null,
    PACKAGE_NAME  TEXT ,
    DATABASE_TYPE TEXT not null,
    CREATE_TIME   TEXT not null
, TYPE TEXT);
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(61,'INTEGER','Integer',NULL,'SQLite','2024-04-20','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(62,'REAL','Double',NULL,'SQLite','2024-04-20','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(63,'TEXT','String',NULL,'SQLite','2024-04-20','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(64,'BLOB','String',NULL,'SQLite','2024-04-20','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(65,'NUMERIC','BigDecimal','java.math.BigDecimal','SQLite','2024-04-20','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(66,'DATE','Date','java.util.Date','SQLite','2024-04-20','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(67,'TIME','Date','java.util.Date','SQLite','2024-04-20','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(68,'DATETIME','Date','java.util.Date','SQLite','2024-04-20','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(69,'INT','Long',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(70,'TINYINT','Integer',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(71,'SMALLINT','Integer',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(72,'MEDIUMINT','Long',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(73,'BIGINT','Long',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(74,'FLOAT','Float',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(75,'DOUBLE','Float',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(76,'DECIMAL','BigDecimal','java.math.BigDecimal','MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(77,'DATE','Date','java.util.Date','MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(78,'TIME','Date','java.util.Date','MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(79,'DATETIME','Date','java.util.Date','MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(80,'TIMESTAMP','Long',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(81,'YEAR','Year','java.time.Year','MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(82,'CHAR','String',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(83,'VARCHAR','String',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(84,'BINARY','String',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(85,'VARBINARY','String',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(86,'TEXT','String',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(87,'ENUM','String',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(88,'SET','String',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(89,'BLOB','String',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(90,'TINYBLOB','String',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(91,'MEDIUMBLOB','String',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(92,'LONGBLOB','String',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(93,'JSON','Object',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(94,'GEOMETRY','Object',NULL,'MySQL','2024-04-21','0');
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(95,'LONGTEXT','String','','MySQL','2024-04-23 12:01:07',NULL);
INSERT INTO MCG_COLUMN_TYPE_MAPPING VALUES(96,'INT','int','','MySQL','2024-04-23 14:00:19',NULL);
CREATE TABLE mcg_code_template (
    id integer primary key ,
    content text,
    package_name text,
    template_name text
, NAME_CONVERTER TEXT, FILE_TYPE TEXT);
INSERT INTO mcg_code_template VALUES(1,replace('#set($author = ${Context.author})\n#set($email = ${Context.email})\n#set($className = ${Context.className})\n#set($basePackage = ${Context.basePackagePath})\n#set($dateTime = ${Context.dateTime})\n#set($description = ${Context.description})\n#set($enableLombok = ${Context.enableLombok})\n#set($columns = ${Context.columns})\n#set($table = ${Context.table})\n#set($originalTableName = ${Context.table.tableName})\n#set($camelTableName = ${Context.table.tableNameCamel})\n#set($cflTableName = ${Context.table.cflTableName})\n#set($serviceName = "${cflTableName}Service")\n#set($serviceVar = "${camelTableName}Service")\n#set($dto = "${cflTableName}Dto")\npackage ${basePackage}.controller;\n\nimport org.springframework.web.bind.annotation.*;\nimport ${basePackage}.dto.${dto};\nimport ${basePackage}.dto.Result;\nimport ${basePackage}.service.${serviceName};\nimport org.springframework.beans.factory.annotation.Autowired;\nimport java.util.List;\n\n/**\n* @Author: $!{author}\n* @Email:  $!{email}\n* @Time:   ${dateTime}\n* @Description: ${basePackage}.dto\n*/\n\n@RestController\n@RequestMapping("/${camelTableName}")\npublic class ${className} {\n\n    @Autowired\n    private ${serviceName} ${serviceVar};\n\n\n    @PostMapping("/queryPage")\n    public Result<IPage<${camelTableName}>> queryPage(@RequestBody $dto dto) {\n        return Result.success(${serviceVar}.queryPage(dto));\n    }\n\n    @PostMapping("/addOrEdit")\n    public Result<Object> addOrEdit(@RequestBody $dto dto) {\n        int n = ${serviceVar}.addOrEdit(dto);\n        return n > 0 ? Result.success() : Result.error();\n    }\n\n    @PostMapping("/batchDeleteById")\n    public Result<Object> batchDeleteById(@RequestBody List<Long> ids) {\n        ${serviceVar}.batchDeleteById(ids);\n        return Result.success();\n    }\n}','\n',char(10)),'controller','controller','${Context.table.cflTableName}Controller','java');
INSERT INTO mcg_code_template VALUES(2,replace('#set($author = ${Context.author})\n#set($email = ${Context.email})\n#set($className = ${Context.className})\n#set($basePackage = ${Context.basePackagePath})\n#set($dateTime = ${Context.dateTime})\n#set($description = ${Context.description})\n#set($enableLombok = ${Context.enableLombok})\n#set($columns = ${Context.columns})\n#set($table = ${Context.table})\n#set($originalTableName = ${Context.table.tableName})\n#set($camelTableName = ${Context.table.tableNameCamel})\n#set($cflTableName = ${Context.table.cflTableName})\npackage ${basePackage}.dto;\n\nimport ${basePackage}.entity.${cflTableName};\n#if($enableLombok)\nimport lombok.Data;\nimport lombok.EqualsAndHashCode;\n#end\n\n/**\n* @Author: $!{author}\n* @Email:  $!{email}\n* @Time:   ${dateTime}\n* @Description: ${basePackage}.dto\n*/\n#if($enableLombok)\n@EqualsAndHashCode(callSuper = true)\n@Data\n#end\npublic class ${className} extends ${table.cflTableName} {\n\n}','\n',char(10)),'dto','dto','${Context.table.cflTableName}Dto','java');
INSERT INTO mcg_code_template VALUES(3,replace('#set($author = ${Context.author})\n#set($email = ${Context.email})\n#set($className = ${Context.className})\n#set($basePackage = ${Context.basePackagePath})\n#set($dateTime = ${Context.dateTime})\n#set($description = ${Context.description})\n#set($camelTableName = ${Context.table.tableNameCamel})\n#set($cflTableName = ${Context.table.cflTableName})\npackage ${basePackage}.mapper;\n\nimport com.baomidou.mybatisplus.core.mapper.BaseMapper;\nimport ${basePackage}.${cflTableName};\n\n/**\n * @Author: $!{author}\n * @Email:  $!{email}\n * @Time:   ${dateTime}\n * @Description: ${basePackage}.mapper\n */\npublic interface ${className} extends BaseMapper<${cflTableName}> {\n    \n}','\n',char(10)),'mapper','mapper','${Context.table.cflTableName}Mapper','java');
INSERT INTO mcg_code_template VALUES(4,replace('#set($author = ${Context.author})\n#set($email = ${Context.email})\n#set($className = ${Context.className})\n#set($basePackage = ${Context.basePackagePath})\n#set($dateTime = ${Context.dateTime})\n#set($description = ${Context.description})\n#set($enableLombok = ${Context.enableLombok})\n#set($columns = ${Context.columns})\n#set($table = ${Context.table})\n#set($originalTableName = ${Context.table.tableName})\n#set($camelTableName = ${Context.table.tableNameCamel})\n#set($cflTableName = ${Context.table.cflTableName})\n#set($dto = "${cflTableName}Dto")\npackage ${basePackage}.service;\n\nimport com.baomidou.mybatisplus.core.metadata.IPage;\nimport ${basePackage}.dto.${dto};\nimport ${basePackage}.entity.${cflTableName};\n\nimport java.util.List;\n\n/**\n * @Author: $!{author}\n * @Email:  $!{email}\n * @Time:   ${dateTime}\n * @Description: ${basePackage}.dto\n */\npublic interface ${className} {\n\n\n    /**\n     * 分页查询\n     *\n     * @param dto 请求参数\n     * @return 分页数据\n     */\n    IPage<${cflTableName}> queryPage(${dto} dto);\n\n    /**\n     * 根据ID存在判断新增或者修改\n     *\n     * @param dto 新增/修改数据\n     * @return int 修改行数\n     */\n    int addOrEdit(${dto} dto);\n\n    /**\n     * 根据ID批量删除\n     *\n     * @param ids 删除数据ID集合\n     */\n    void batchDeleteById(List<Long> ids);\n\n    /**\n     * 查询List\n     *\n     * @param dto 查询条件DTO\n     * @return 返回数据\n     */\n    List<${cflTableName}> list(${cflTableName} dto);\n}\n','\n',char(10)),'service','service','${Context.table.cflTableName}Service','java');
INSERT INTO mcg_code_template VALUES(5,replace('#set($author = ${Context.author})\n#set($email = ${Context.email})\n#set($className = ${Context.className})\n#set($basePackage = ${Context.basePackagePath})\n#set($dateTime = ${Context.dateTime})\n#set($description = ${Context.description})\n#set($enableLombok = ${Context.enableLombok})\n#set($columns = ${Context.columns})\n#set($table = ${Context.table})\n#set($originalTableName = ${Context.table.tableName})\n#set($camelTableName = ${Context.table.tableNameCamel})\n#set($cflTableName = ${Context.table.cflTableName})\n#set($serviceName = "${cflTableName}Service")\n#set($serviceVar = "${camelTableName}Service")\n#set($dto = "${cflTableName}Dto")\n#set($mapperName = "${cflTableName}Mapper")\n#set($mapperVar = "${camelTableName}Mapper")\npackage ${basePackage}.service.impl;\n\nimport com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;\nimport com.baomidou.mybatisplus.core.metadata.IPage;\nimport com.baomidou.mybatisplus.extension.plugins.pagination.Page;\nimport com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;\nimport ${basePackage}.dto.Result;\nimport ${basePackage}.dto.${dto};\nimport ${basePackage}.mapper.${mapperName};\nimport ${basePackage}.service.${serviceName};\nimport ${basePackage}.entity.${cflTableName};\nimport org.springframework.stereotype.Service;\nimport org.springframework.transaction.annotation.Transactional;\nimport org.springframework.util.CollectionUtils;\nimport org.springframework.util.ObjectUtils;\nimport org.springframework.beans.factory.annotation.Autowired;\n\nimport java.util.List;\n\n/**\n * @Author: $!{author}\n * @Email:  $!{email}\n * @Time:   ${dateTime}\n * @Description: ${basePackage}.dto\n */\n\n@Service\npublic class ${className} extends ServiceImpl<${mapperName}, ${cflTableName}> implements ${serviceName} {\n\n    @Autowired\n    private ${mapperName} ${mapperVar};\n\n    @Override\n    public IPage<${cflTableName}> queryPage(${dto} dto) {\n        Page<${cflTableName}> page = new Page<>(dto.getPageNum(), dto.getPageSize());\n        LambdaQueryWrapper<${cflTableName}> wrapper = buildQueryWrapper(dto);\n        return ${mapperVar}.selectPage(page, wrapper);\n    }\n\n    @Override\n    @Transactional\n    public int addOrEdit(${dto} dto) {\n        if (ObjectUtils.isEmpty(dto.getId())) {\n            return ${mapperVar}.insert(dto);\n        } else {\n            return ${mapperVar}.updateById(dto);\n        }\n    }\n\n    @Override\n    @Transactional\n    public void batchDeleteById(List<Long> ids) {\n        if (CollectionUtils.isEmpty(ids)) {\n            return;\n        }\n        this.removeBatchByIds(ids);\n    }\n\n    @Override\n    public List<${cflTableName}> list(${dto} dto) {\n        LambdaQueryWrapper<${cflTableName}> wrapper = buildQueryWrapper(qo);\n        return ${mapperVar}.selectList(wrapper);\n    }\n\n\n    private LambdaQueryWrapper<${cflTableName}> buildQueryWrapper(${dto} dto) {\n        LambdaQueryWrapper<ColumnTypeMapping> wrapper = new LambdaQueryWrapper<>();\n        // TODO build you query wrapper here\n        return wrapper;\n    }\n}','\n',char(10)),'service.impl','serviceImpl','${Context.table.cflTableName}ServiceImpl','java');
INSERT INTO mcg_code_template VALUES(6,replace('#set($author = ${Context.author})\n#set($email = ${Context.email})\n#set($className = ${Context.className})\n#set($basePackage = ${Context.basePackagePath})\n#set($dateTime = ${Context.dateTime})\n#set($description = ${Context.description})\n#set($enableLombok = ${Context.enableLombok})\n#set($columns = ${Context.columns})\n#set($table = ${Context.table})\n#set($originalTableName = ${Context.table.tableName})\n#set($camelTableName = ${Context.table.tableNameCamel})\n#set($cflTableName = ${Context.table.cflTableName})\n#set($serviceName = "${cflTableName}Service")\n#set($serviceVar = "${camelTableName}Service")\npackage ${basePackage}.entity;\n\nimport com.baomidou.mybatisplus.annotation.TableId;\nimport com.baomidou.mybatisplus.annotation.TableName;\nimport com.baomidou.mybatisplus.annotation.TableField;\n#foreach($package in ${table.columnDataTypePackage})\nimport ${package};\n#end\n\n#if($enableLombok)\nimport lombok.Data;\n#end\n\n/**\n * @Author: $!{author}\n * @Email:  $!{email}\n * @Time:   ${dateTime}\n#if("$!table.tableComment" == "")\n * @Description: ${basePackage}.dto\n#else\n * @Description: ${table.tableComment}\n#end\n */\n\n#if($enableLombok)\n@Data\n#end\n@TableName(value = "${originalTableName}")\npublic class ${cflTableName}{\n\n#foreach($column in ${columns})\n\n    /**\n    #if("$!{column.columnComment}" == "")\n    "* ${column.originalColumnName}"\n    #else\n    "* ${column.columnComment}"\n    #end\n    */\n    #if(${column.primaryKey})\n    @TableId\n    #else\n    @TableField("${column.originalColumnName}")\n    #end\n    private ${column.columnType} ${column.camelTypeColumnName};\n#end\n\n#if(!$enableLombok)\n    #foreach(${column} in ${columns})\n    public void set${column.cflColumnName}(${column.columnType} ${column.camelTypeColumnName}) {\n        this.${column.camelTypeColumnName} = ${column.camelTypeColumnName};\n    }\n\n    public ${column.columnType} get${column.cflColumnName}() {\n        return this.${column.camelTypeColumnName};\n    }\n\n    #end\n#end\n}\n','\n',char(10)),'entity','entity','${Context.table.cflTableName}','java');
INSERT INTO mcg_code_template VALUES(7,replace('#set($className = ${Context.className})\n#set($basePackage = ${Context.basePackagePath})\n#set($columns = ${Context.columns})\n#set($originalTableName = ${Context.table.tableName})\n#set($cflTableName = ${Context.table.cflTableName})\n<?xml version="1.0" encoding="UTF-8" ?>\n<!DOCTYPE  mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >\n<mapper namespace="${basePackage}.mapper.${className}">\n    <resultMap id="BaseResultMap" type="${basePackage}.entity.${cflTableName}">\n    #foreach($column in $columns)\n        <result column="${column.originalColumnName}" property="${column.camelTypeColumnName}" />\n    #end\n    </resultMap>\n\n    <!-- 表字段 -->\n    <sql id="baseColumns">\n        #foreach($column in $columns)\n            t.${column.originalColumnName}#if($foreach.hasNext),#end\n        #end\n    </sql>\n\n\n</mapper>','\n',char(10)),'mapper.xml','mapper.xml','${Context.table.cflTableName}Mapper','xml');
CREATE TABLE MCG_SYSTEM_PARAMETER
(
    ID     INTEGER primary key,
    PARAM_KEY TEXT not null,
    PARAM_VALUE REAL,
    PARAM_TYPE TEXT,
    ENABLE TEXT,
    PARAM_REMARK  TEXT
, TYPE  TEXT);
INSERT INTO MCG_SYSTEM_PARAMETER VALUES(1,'项目包名','com.mcg','1','Y','项目包路径','SYS');
INSERT INTO MCG_SYSTEM_PARAMETER VALUES(2,'去掉表前缀','MCG','1','Y','生成代码是是否去掉前缀','SYS');
INSERT INTO MCG_SYSTEM_PARAMETER VALUES(3,'作者名','meimw','1','Y','Author','SYS');
INSERT INTO MCG_SYSTEM_PARAMETER VALUES(4,'邮箱','1251835319@qq.com','1','Y','email','SYS');
INSERT INTO MCG_SYSTEM_PARAMETER VALUES(5,'启用Lombok','Y','0','Y','是否启用Lombok','SYS');
COMMIT;
