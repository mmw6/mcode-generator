# MCG代码生成器

## 项目说明

一款低代码生成器，支持自定义模板，自定义字段类型映射等功能。快速生成共性代码

## 功能特点

- 提供了常见的几种内置模板，并且模板可自定义
- 提供了不同数据库的字段类型映射
- 数据源支持: SQLite，MySQL，其他类型数据源后续更新中......
- 内置SQLite数据库，使用无需额外配置，下载即可使用

## 开发环境

- 后端

  Java8  Maven3,sqlite3

- 前端

  node,vue3
  前端基于[vue-next-admin](https://gitee.com/lyt-top/vue-next-admin)框架开发, node版本需要 14.18+ / 16+
## 使用方法

- 下载发行版

- 解压缩jar包后，点击start.bat启动
- 在浏览器打开 [http://localhost:8080/gen/](http://localhost:8080/gen)

## 效果图
![输入图片说明](preview/preview1.png)
![输入图片说明](preview/preview2.png)
![输入图片说明](preview/preview3.png)
![输入图片说明](preview/preview4.png)

## 进行中
1. 完善其他数据源代码生成功能
2. 代码生成预览功能